# Setapp Auto-Timesheet

## Requirements
* dotnet core 2.0 (http://dot.net/)

## Build & Run
`dotnet restore`

`dotnet build`

`dotnet run`

## Publish
`dotnet publish -c release -r win-x64 -o outputDir`

## Usage
`SetappAutoTimesheet.exe "your.username" "yourpassword"`