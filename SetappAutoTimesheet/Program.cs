﻿using OpenQA.Selenium.Chrome;
using System;
using System.IO;
using System.Reflection;

namespace SetappAutoTimesheet
{
    internal class Program
    {
        private const string _timesheetUrl = "https://timesheet.setapp.pl";

        private static void Main(string[] args)
        {
            var credentials = ParseCredentialsFromArguments(args);
            args = null;

            using (ChromeDriver driver = CreateDriver())
            {
                driver.Navigate().GoToUrl(_timesheetUrl);
                driver.FindElementById("username").SendKeys(credentials.username);
                driver.FindElementById("password").SendKeys(credentials.password);
                driver.FindElementById("_submit").Click();
                driver.FindElementByClassName("btn-success").Click();
                driver.Navigate().GoToUrl($"{_timesheetUrl}/logout");

                driver.Close();
            }
        }

        private static ChromeDriver CreateDriver()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("--headless");
            string chromeDriverLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            return new ChromeDriver(chromeDriverLocation, chromeOptions);
        }

        public static (string username, string password) ParseCredentialsFromArguments(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Please pass username and password as arguments");
                Environment.Exit(-1);
            }

            return (username: args[0], password: args[1]);
        }
    }
}